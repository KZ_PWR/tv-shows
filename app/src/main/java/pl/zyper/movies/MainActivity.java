package pl.zyper.movies;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    public static List<MovieInfo> movies;
    private MoviesDatabase base;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set layout manager
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        // create database and load random number of movies
        base = new MoviesDatabase();
        movies = base.getList(10+(int)(Math.random()*15));


        setRecyclerView();
        setAddButton();

        updateTitle();
    }

    private void setAddButton() {
        FloatingActionButton fab = findViewById(R.id.add_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MovieInfo movie = base.getList(1).get(0);
                Toast.makeText(MainActivity.this, getString(R.string.added_msg) + " " + movie.title, Toast.LENGTH_SHORT).show();
                movies.add(movie);
                recyclerView.getAdapter().notifyDataSetChanged();
                updateTitle();
            }
        });
    }

    private void setRecyclerView()
    {
        // set parameters of recycler view
        recyclerView = findViewById(R.id.movie_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(32);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(new MovieListAdapter(movies));
        recyclerView.invalidate();

        // add swipe left/right support
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                // delete element on swipe
                Toast.makeText(MainActivity.this, getString(R.string.deleted_msg) + " " + movies.get(position).title, Toast.LENGTH_SHORT).show();
                movies.remove(position);
                recyclerView.getAdapter().notifyDataSetChanged();

                updateTitle();
            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void updateTitle()
    {
        // print number of movies on title bar
        setTitle(getResources().getString(R.string.app_name) + " (" + movies.size() + ")");
    }
}
