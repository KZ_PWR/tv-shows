package pl.zyper.movies;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


public class PhotoFragment extends MovieDataFragment {

    public PhotoFragment() {}

    public static PhotoFragment newInstance(int movieID) {
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(makeBundle(movieID));
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        GridView photosGrid = view.findViewById(R.id.photos_grid);
        photosGrid.setAdapter(new PhotosAdapter(getActivity()));
    }

    private class PhotosAdapter extends BaseAdapter
    {
        private final Context context;
        PhotosAdapter(Context context)
        {
            super();
            this.context = context;
        }

        @Override
        public int getCount() {
            return movie.photos.length;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView image;
            if (convertView == null)
            {
                image = new ImageView(context);

            } else {
                image = (ImageView)convertView;
            }

            image.setScaleType(ImageView.ScaleType.FIT_XY);
            image.setAdjustViewBounds(true);
            image.setMinimumHeight(parent.getHeight()/2);

            image.setImageBitmap(movie.photos[position]);
            return image;
        }

    }

}
