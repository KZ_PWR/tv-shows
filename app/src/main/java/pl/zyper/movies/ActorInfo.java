package pl.zyper.movies;

import android.graphics.Bitmap;

public class ActorInfo {
    /* First name and last name */
    final String name;

    /* Photo of the actor */
    Bitmap photo;

    ActorInfo()
    {
        this.name = "";
        this.photo = null;
    }

    ActorInfo(String name)
    {
        this.name = name;
        this.photo = null;
    }

    ActorInfo(String name, Bitmap photo)
    {
        this.name = name;
        this.photo = photo;
    }
}
