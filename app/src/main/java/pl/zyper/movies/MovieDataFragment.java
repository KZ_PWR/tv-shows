package pl.zyper.movies;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;

public class MovieDataFragment extends Fragment {
    private static final String ARG_MOVIE_ID = "MovieID";
    MovieInfo movie;

    @NonNull
    static Bundle makeBundle(int movieID) {
        Bundle args = new Bundle();
        args.putInt(ARG_MOVIE_ID, movieID);
        return args;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // get movie from movies list based on movieID from bundle
            movie = MainActivity.movies.get(getArguments().getInt(ARG_MOVIE_ID));
        }
        if (movie == null)
        {
            Log.e("FRAGMENT", "MovieInfo is null!");
        }
    }


}
