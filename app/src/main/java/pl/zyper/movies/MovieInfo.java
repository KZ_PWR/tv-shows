package pl.zyper.movies;

import android.graphics.Bitmap;

public class MovieInfo {
    final String title;
    final String genre;
    final Bitmap cover;
    final Bitmap[] photos;
    final ActorInfo[] actors;

    MovieInfo(String title, String genre, Bitmap cover, Bitmap[] photos, ActorInfo[] actors)
    {
        this.title = title;
        this.genre = genre;

        this.cover = cover;

        this.photos = photos;
        this.actors = actors;
    }
}