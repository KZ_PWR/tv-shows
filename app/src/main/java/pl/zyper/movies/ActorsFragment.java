package pl.zyper.movies;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ActorsFragment extends MovieDataFragment {

    public ActorsFragment() {}

    public static ActorsFragment newInstance(int movieID) {
        ActorsFragment fragment = new ActorsFragment();
        fragment.setArguments(makeBundle(movieID));
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_actors, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        // set adapter of actors_list
        ListView actorsList = view.findViewById(R.id.actors_list);
        actorsList.setAdapter(new ActorsAdapter(getActivity()));
    }

    private class ActorsAdapter extends BaseAdapter
    {
        private final Context context;
        ActorsAdapter(Context context)
        {
            super();
            this.context = context;
        }

        @Override
        public int getCount() {
            return movie.actors.length;
        }

        @Override
        public ActorInfo getItem(int position) {
            return movie.actors[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {

            if (convertView == null)
            {
                convertView = LayoutInflater.from(context).inflate(R.layout.actor_item, parent, false);
            }
            TextView name = convertView.findViewById(R.id.actor_name);
            ImageView image = convertView.findViewById(R.id.actor_image);

            name.setText(getItem(position).name);
            image.setImageBitmap(getItem(position).photo);

            return convertView;
        }
    }
}
