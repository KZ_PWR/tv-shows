package pl.zyper.movies;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    private final static String INTENT_MOVIE_ID = "intentMovieID";

    /* width and height of header image */
    private final int BITMAP_WIDTH = 300;
    private final int BITMAP_HEIGHT = 180;

    /* number of pages in ViewPager */
    private final int NUM_PAGES = 2;

    private int movieID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // get movieID from bundle
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            movieID = bundle.getInt(INTENT_MOVIE_ID);
        }

        // fill text/image views with data
        fillElementsWithData(MainActivity.movies.get(movieID));

        // set viewpager for photos and actors fragments
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new MovieDataAdapter(getSupportFragmentManager()));

        // set title bar to movie title
        setTitle(MainActivity.movies.get(movieID).title);
    }

    public static void start(Context context, int movieId) {
        Intent starter = new Intent(context, InfoActivity.class);
        starter.putExtra(INTENT_MOVIE_ID, movieId);
        context.startActivity(starter);
    }

    private void fillElementsWithData(MovieInfo movie)
    {
        ImageView image = findViewById(R.id.movie_image);
        TextView title = findViewById(R.id.movie_title);
        TextView category = findViewById(R.id.movie_category);

        image.setImageBitmap(Bitmap.createBitmap(movie.cover, 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT) );

        title.setText(movie.title);
        category.setText(movie.genre);
    }

    private class MovieDataAdapter extends FragmentStatePagerAdapter {
        MovieDataAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 1:
                    return ActorsFragment.newInstance(movieID);
                default:
                case 0:
                    return PhotoFragment.newInstance(movieID);

            }
        }

        @Override
        public int getCount()
        {
            return NUM_PAGES;
        }
    }
}
