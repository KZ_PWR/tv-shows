package pl.zyper.movies;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {

    private static final int BITMAP_WIDTH = 136;
    private static final int BITMAP_HEIGHT = 200;
    private final List<MovieInfo> list;

    MovieListAdapter(List<MovieInfo> list)
    {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                         int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.entry, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(list.get(position).title);
        holder.genre.setText(list.get(position).genre);

        if (list.get(position).cover != null) {
            holder.cover.setImageBitmap(Bitmap.createScaledBitmap(list.get(position).cover,
                                                    BITMAP_WIDTH, BITMAP_HEIGHT, false));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        final TextView title;
        final TextView genre;
        final ImageView cover;

        ViewHolder(View parent) {
            super(parent);

            this.title = parent.findViewById(R.id.movie_title);
            this.genre = parent.findViewById(R.id.movie_genre);
            this.cover = parent.findViewById(R.id.movie_image);

            parent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            InfoActivity.start(v.getContext(), getAdapterPosition());

        }
    }
}
